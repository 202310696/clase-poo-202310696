<?php

class Teclado{

    public $dimensiones = "";
    public $peso = "";
    public $conexion = "";

    public function setDimensiones($dimensiones){
        $this->dimensiones = $dimensiones;
    }

    public function getDimensiones(){
        return $this->dimensiones;
    }
    public function setPeso($peso){
        $this->peso = $peso;
    }

    public function getPeso(){
        return $this->peso;
    }
    public function setConexion($conexion){
        $this->conexion = $conexion;
    }

    public function getConexion(){
        return $this->conexion;
    }

    }
    $objHyperx = new Teclado();

    $objHyperx->setDimensiones("20x40x5");
    $objHyperx->setPeso("800 gr");
    $objHyperx->setConexion("USB");


    echo "Dimensiones del teclado:". $objHyperx->getDimensiones()."<br>";

    echo "Peso del Teclado :". $objHyperx->getPeso()."<br>";

    echo "Tipo de Connexion del Teclado:". $objHyperx->getConexion()."<br>";

?>



 
