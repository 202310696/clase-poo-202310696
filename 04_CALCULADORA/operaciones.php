<?php

//Clase operaciones
class Operaciones{

    //Atributo Operacion a realizar
    public $OperacionRealizar = "";


    //Método Suma con parámetros a y b
    public function Suma($a,$b){
        return $a + $b;
    }

    //Método Resta con parámetros a y b
    public function Resta($a,$b){
        return $a - $b;
    }

    //Método División con parámetros a y b
    public function Division($a,$b){
        return $a / $b;
    }
    
    //Método Multiplicación con parámetros a y b
    public function Multiplicacion($a,$b){
        return $a * $b;
    }

    //Método que ejecuta los metodos en base a la propiedad $OperacionRealizar y tienen dos parámetros a y b
    public function ResultadoOperacion($a,$b){

        switch ($this->OperacionRealizar) {
            case 'suma':
                return $this->Suma($a,$b);
                break;
            case 'resta':
                return $this->Resta($a,$b);
                break;
            case 'division':
                return $this->Division($a,$b);
                break;
            case 'multiplicacion':
                return $this->Multiplicacion($a,$b);
                break;    
            default:
                return 'Operacion a realizar no Definida';
                break;
        }

    }

}



?>