<?php


abstract Class Transporte{


    abstract protected function Mantenimiento();

}

Class Avion Extends Transporte{

    public function Mantenimiento(){
        echo "</br>1.- Mantenimiento a un avion:</br>";
        echo "</br>Revisiones en tránsito</br>";
        echo "</br>Revisiones diarias</br>";
        echo "</br>Revisiones de 48 horas</br>";
    }

}

Class Automovil extends Transporte{

    public function Mantenimiento(){
        echo "</br>2.- Mantenimiento a un Automovil:</br>";
        echo "</br>Cambio de filtros y de aceite</br>";
        echo "</br>Revisión de frenos</br>";
        echo "</br>Revisión de neumáticos</br>";
    }

}

Class Bicicleta extends Transporte{

    public function Mantenimiento(){
        echo "</br>3.- Mantenimiento a una bicicleta</br>";
        echo "</br>Revisión de transmisión</br>";
        echo "</br>Revisión de frenos</br>";
        echo "</br>Revisión de ruedas</br>";
    }


}

Class Motocicleta extends Transporte{

    public function Mantenimiento(){
        echo "</br>4.- Mantenimiento a una motocicleta:</br>";
        echo "</br>Revisión de batería</br>";
        echo "</br>Comprobar las pastillas de freno y la cadena</br>";
        echo "</br>Limpieza y lubricación</br>";
    }


}

Class Camion extends Transporte{

    public function Mantenimiento(){
        echo "</br>5.- Mantenimiento a un camion:</br>";
        echo "</br>Revisar bujías</br>";
        echo "</br>Revisión de filtro de aceite</br>";
        echo "</br>Revisión de aceite</br>";
    }

}
$obj = new Avion();
$obj2 = new Automovil();
$obj3 = new Bicicleta();
$obj4 = new Motocicleta();
$obj5 = new Camion();
$obj->Mantenimiento();
$obj2->Mantenimiento();
$obj3->Mantenimiento();
$obj4->Mantenimiento();
$obj5->Mantenimiento();
?>