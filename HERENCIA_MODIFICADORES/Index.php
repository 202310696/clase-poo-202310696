<?php

Class EjemploModificadores{

    public function metodo1(){
        echo "Este es el método 1";
    }

    protected function metodo2(){
        echo "Este es el método 2";
    }

    private function metodo3(){
        echo "Este es el metodo 3";    
    }

    public function AccesoMetodo3(){
        $this->metodo3();
    }

    public function FirmaAlumno(){
        return "El alumno a confirmado los datos y firmado";
    }
}

Class Hijo extends EjemploModificadores{

    public function AccesoMetodo2(){
        $this->metodo2();
    }

    public function Firmar(){
        return $this->FirmaAlumno();
    }


}


 $obj = new EjemploModificadores;
 $obj = new Hijo;
 $obj->metodo1();
 $obj->AccesoMetodo2();
 $obj->AccesoMetodo3();






?>