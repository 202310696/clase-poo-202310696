<?php

Class Alumno{

    public function __constructor($nombre){
        $nombre = "Christian";
        echo " Datos de el Alumno ";
    }
    
    public function NombreAlumno(){
        $nombre = "Christian";
        echo " </br> Este es el Nombre del Alumno es: " .$nombre;
    }

    protected function EdadAlumno(){
        $edad = "18 años";
        echo " </br>Esta es la edad del Alumno es: ".$edad;
    }

    private function CarreraCurso(){
        $carrera = "Infomratica";
        echo " </br>Esta es la carrera en curso es : ".$carrera;    
    }

    public function AccesoCarrera(){
        $this->CarreraCurso();
    }
    

    public function FirmaAlumno(){
        echo "</br>El alumno a confirmado los datos y firmado</br>";
    }

    public function destruct__(){
        echo "Datos del Alumno";
    }
}

Class Hijo extends Alumno{

    public function AccesoEdad(){
        $this->EdadAlumno();
    }

    public function Firmar(){
        return $this->FirmaAlumno();
    }


}

 $obj = new Alumno;
 $obj = new Hijo;
 $obj->NombreAlumno();
 $obj->AccesoCarrera();
 $obj->AccesoEdad();
 $obj->Firmar();