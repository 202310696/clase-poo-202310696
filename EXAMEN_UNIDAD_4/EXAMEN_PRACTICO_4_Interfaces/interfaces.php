<?php

interface Areas{
    public function Rectangulo($a,$b);
    public function Triangulo($a,$b);

}

Class Area implements Areas{
    public function Rectangulo($a,$b){
        return $a * $b;
    }

    public function Triangulo($a,$b){
       return $a * $b / 2;
    }
}

?>