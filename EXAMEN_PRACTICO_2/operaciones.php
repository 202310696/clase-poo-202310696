<?php

//Clase Areas
class Areas{

    //Funcion en privado es OperacionArea que es el modificador de acceso restringido
    public $OperacionArea = "";
    // Se crea un constructor con _construct()
    function _construct($OperacionArea){
        $this->OperacionArea = $OperacionArea;    
    }

    //Método para calcular el área del circulo
    public function circulo($radio){
        return ($radio ** 2) *M_PI;
    }
 
    //Método para calcular el área del cuadrado
    public function cuadrado($lado){
        return ($lado * $lado);
    }

    //Método para calcular el área del triangulo
    public function triangulo($base,$altura){
        return $base * $altura / 2;
    }

    //Método para calcular el área del rectangulo
    public function rectangulo($base,$altura){
        return $altura * $base;
    }

    //Método para calcular el área del rombo
    public function rombo($dmayor,$dmenor){
        return $dmayor * $dmenor / 2;
    }

    //Método que ejecuta los metodos en base a la propiedad $OperacionArea con los parametros ($radio,$lado,$base,$altura,$dmenor,$dmayor)
    public function ResultadoOperacion($radio,$lado,$base,$altura,$dmenor,$dmayor){

        switch ($this->OperacionArea) {
            case 'circulo':
                return $this->circulo($radio);
                break;  
            case 'cuadrado':
                return $this->cuadrado($lado);
                break;
            case 'triangulo':
                return $this->triangulo($base,$altura);
                break;  
            case 'rectangulo':
                return $this->rectangulo($base,$altura);
                break;       
            case 'rombo':
                return $this->rombo($dmayor,$dmenor);
                break; 
            default:
                return 'Area a realizar no Definida';
                break;
        }

    }

}