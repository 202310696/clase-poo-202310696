<?php

interface OperacionesGenerales{
    public function Redondeo($monto);
    public function CalcularIVA($monto);

}

Class PuntoDeVenta implements OperacionesGenerales{
    public function Redondeo($monto){
        return round($monto);
    }

    public function CalcularIVA($monto){
       return $IVA = $monto * .16;
    }
}

?>